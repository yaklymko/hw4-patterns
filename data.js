const getRandomElementFromReturnList = {
  apply(target, thisArg, argumentsList) {
    const resultArray = target(argumentsList[0]);
    return resultArray[Math.floor(Math.random() * resultArray.length)];
  },
};

function proxyWrapper(func) {
  return new Proxy(func, getRandomElementFromReturnList);
}

export const CommentBotPhrases = new Map([
  [
    'USER_JOINED',

    [proxyWrapper(({ username }) => [
      `Yo, ${username}!`,
      `Howdy, ${username}`,
      `Hey, Hey mister ${username}`,
      `Say hi to ${username}`,
    ])],
  ],

  [
    'LEAVE_ROOM',

    [proxyWrapper(({ username }) => [
      `Bye, ${username}!`,
      `See ya, ${username}`,
      `${username} is gonna on his business`,
      `Say goodbye to mister ${username}. He's gonna be back soon!`,
    ])],
  ],
  [
    'START_TIMER',

    [proxyWrapper(() => [
      'Greetings lads! Im all in you service to the very end of the match',
      'Prepare to the battle!',
      'Lets do this pals, proof yourself!',
    ])],
  ],
  [
    'UPDATE_GAME',

    [proxyWrapper(({ users }) => {
      const sortedUsers = users.sort((a, b) => b.progress - a.progress);
      return [
        `Oh my god!!!! Could someone stop this Moster [${sortedUsers[0].username}? He is so fast!]`,
        `Yo, Mr[${sortedUsers[sortedUsers.length - 1].username}], you'd better hurry up!`,
        `We see player [${sortedUsers[0].username}] beating everybody. Will [${sortedUsers[1] || 'someone'}] stop him?????`,
      ];
    })],
  ],

  [
    'PRE_GAME_TIMER',

    [proxyWrapper(({ timer }) => [
      `YOU READY???  [${timer}]`,
      `PREPARE YOU FINGERS  [${timer}]`,
      `STEADY  [${timer}]`,
      `LET"S GO! [${timer}]`,
    ])],
  ],

  [
    'PLAYER_FINISHED',

    [proxyWrapper(({ player }) => [
      `Player [${player.username}] finished!`,
      `Hurry up, [${player.username}] finished already`,
    ])],
  ],

  [
    'PLAYER_FINISHING',

    [proxyWrapper(({ player }) => [
      `Player [${player.username}] is almost at the end!`,
      `Hurry up, [${player.username}] is going to finish`,
      `[${player.username}] has only 30 chars left`,
    ])],
  ],

]);

export const texts = [
  '1Lorem ipsum dolor sit amet, consectetuLorem ipsum dolor sit amet, consectetuLorem ipsum dolor sit amet, consectetuLorem ipsum dolor sit amet, consectetuLorem ipsum dolor sit amet, consectetuLorem ipsum dolor sit amet, consectetu',
  '2Lorem ipsum dolor sit amet, consectetuLorem ipsum dolor sit amet, consectetuLorem ipsum dolor sit amet, consectetuLorem ipsum dolor sit amet, consectetu',
  '3Lorem ipsum dolor sit amet, consectetuLorem ipsum dolor sit amet, consectetuLorem ipsum dolor sit amet, consectetu',
];

export default { texts, CommentBotPhrases };
