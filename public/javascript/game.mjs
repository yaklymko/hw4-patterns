import {
  createFinishersModalText,
  createGameText,
  renderPreGameTimer,
  renderRoomInfo,
  renderRoomsList,
  setGameTimer,
  setPageGame,
  setPageRooms,
  setTextBlock,
  setTimerBlock,
  setCommentsBlock,
} from './gameRenders.mjs';

import { deinitKeyboardListeners, getGameText, initKeyboardListeners } from './gameController.mjs';

const username = sessionStorage.getItem('username');
const roomsPage = document.getElementById('rooms-page');
const gamePage = document.getElementById('game-page');
const timerBlock = gamePage.getElementsByClassName('timer-block')[0];

if (!username) {
  sessionStorage.clear();
  window.location.replace('/login');
}

const createRoomButton = document.getElementById('createRoomButton');
createRoomButton.addEventListener('click', (event) => {
  const roomName = prompt('How do you want to call your room?');
  if (!roomName) {
    return;
  }
  socket.emit('CREATE_ROOM', roomName);
});

const leaveRoomButton = document.getElementById('game-leave-button');
leaveRoomButton.addEventListener('click', (event) => {
  socket.emit('LEAVE_ROOM');
});

const socket = io('', { query: { username } });

socket.on('ROOM_JOINED', (room) => {
  setPageGame(roomsPage, gamePage);
  renderRoomInfo(room, socket);
  setGameTimer(false);
});

socket.on('UPDATE_CURR_ROOM', (room) => {
  renderRoomInfo(room, socket);
});

socket.on('ROOM_LEFT', () => {
  setPageRooms(roomsPage, gamePage);
});

socket.on('auth_error', () => {
  alert('User already registered');
  sessionStorage.removeItem('username');
  window.location.replace('/login');
});

socket.on('UPDATE_ROOMS', (rooms) => {
  renderRoomsList(rooms, roomsPage, socket);
});

socket.on('ERR_INVALID_ROOM_NAME', () => {
  alert('This name is already used');
});

socket.on('START_TIMER', (textId) => {
  setTimerBlock(true);
  setTextBlock(false);
  getGameText(textId).then((text) => {
    createGameText(text);
  });
});

socket.on('SECONDS_BEFORE_START', (seconds) => {
  renderPreGameTimer(seconds, timerBlock);
});

socket.on('START_GAME', (room) => {
  renderRoomInfo(room, socket);
  setTimerBlock(false);
  setTextBlock(true);
  initKeyboardListeners(socket);
});

socket.on('FINISH_GAME', (finishers) => {
  setTimerBlock(false);
  setTextBlock(false);
  setCommentsBlock(true, createFinishersModalText(finishers));
  deinitKeyboardListeners(socket);
  setGameTimer(false);

  const commentatorDelay = 6000;
  setTimeout(() => setCommentsBlock(false), commentatorDelay);
});

socket.on('GAME_SECONDS_LEFT', (secondsLeft) => {
  setGameTimer(true, secondsLeft);
});

socket.on('COMMENT_BOT', (data) => {
  setCommentsBlock(true, data.text);
  console.log(data);
  const delay = data.delay || 2000;
  if (data.delay !== -1) {
    setTimeout(() => setCommentsBlock(false), delay);
  }
});
