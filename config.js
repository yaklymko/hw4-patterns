// set up env vars

import path from 'path';

require('dotenv').config({ path: `${__dirname}/.env` });

export const STATIC_PATH = path.join(__dirname, 'public');
export const HTML_FILES_PATH = path.join(STATIC_PATH, 'html');

export const PORT = process.env.PORT || 3001;
