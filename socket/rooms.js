import { texts } from '../data';
import { SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME } from './config';

export default (socket, username, usersArr, roomsController, socketHelper, observer) => {
  socket.on('JOIN_ROOM', (roomId) => {
    const prevRoomId = roomsController.getCurrentRoomId(socket);

    if (roomId === prevRoomId) {
      return;
    }

    if (prevRoomId) {
      socket.leave(prevRoomId);
    }

    socket.join(roomId, () => {
      const roomInfo = roomsController.getRoomById(roomId);
      roomInfo.players.push({ username, ready: false });

      const roomsArray = roomsController.getRoomsArray();
      socketHelper.onPlayerJoin(roomId, roomInfo, roomsArray);

      observer.invoke('USER_JOINED', { roomId, username });
    });
  });

  socket.on('LEAVE_ROOM', () => {
    const currRoomId = roomsController.getCurrentRoomId(socket);
    socket.leave(currRoomId);
    const currRoom = roomsController.getRoomById(currRoomId);
    const finishersArr = currRoom.finishers || [];
    roomsController.deletePlayerByRoomId(username, currRoomId);

    if (!currRoom.players.length) {
      roomsController.deleteRoomById(currRoomId);
    } else {
      socket.to(currRoomId).emit('UPDATE_CURR_ROOM', { name: currRoomId, ...currRoom });

      if (roomsController.arePlayersReady(currRoomId)) {
        const randomId = Math.floor(Math.random() * texts.length);

        roomsController.setStatePreTimer(currRoomId, texts[randomId].length);

        const roomArray = roomsController.getRoomsArray();
        socketHelper.startPreGameTimer(SECONDS_TIMER_BEFORE_START_GAME, randomId,
          currRoomId, roomArray,
          () => {
            roomsController.startGame(currRoomId);
            socketHelper.emitToRoom('START_GAME', { name: currRoomId, ...currRoom }, currRoomId);
            socketHelper.startGameTimer(SECONDS_FOR_GAME, currRoomId, currRoom, () => {
              roomsController.setFinishersArrByProgress(currRoomId);
              roomsController.finishGame(currRoomId);
              socketHelper.finishGame(currRoomId, currRoom, roomArray, finishersArr);
            });
          });
      }
    }

    const roomArray = roomsController.getRoomsArray();
    socketHelper.onPlayerLeave(roomArray);
    observer.invoke('LEAVE_ROOM', { roomId: currRoomId, username });
  });

  socket.on('CREATE_ROOM', (roomName) => {
    let roomArray = roomsController.getRoomsArray();

    if (roomsController.getRoomById(roomName)) {
      socket.emit('ERR_INVALID_ROOM_NAME', roomArray);
      return;
    }

    roomsController.addRoom(roomName, { players: [{ username, ready: false }], status: 'lobby' });

    socket.join(roomName, () => {
      const roomInfo = roomsController.getRoomById(roomName);

      socket.emit('ROOM_JOINED', { name: roomName, ...roomInfo });
      socketHelper.emitToRoom('UPDATE_CURR_ROOM', { name: roomName, ...roomInfo }, roomName);

      roomArray = roomsController.getRoomsArray();
      socket.broadcast.emit('UPDATE_ROOMS', roomArray);
    });
  });

  socket.on('disconnect', () => {
    // When disconnecting we loose all rooms from socket so need to find room id
    const roomId = roomsController.getRoomIdByPlayer(username);

    if (roomId) {
      socket.leave(roomId);

      roomsController.deletePlayerByRoomId(username, roomId);

      const currRoom = roomsController.getRoomById(roomId);

      if (!currRoom.players.length) {
        roomsController.deleteRoomById(roomId);
      }

      socket.to(roomId).emit('UPDATE_CURR_ROOM', { name: roomId, ...currRoom });

      if (currRoom.status !== 'lobby') {
        if (currRoom.finishers) {
          roomsController.removeFromFinishers(username, roomId);
        }
        if (roomsController.arePlayersFinished(roomId)) {
          const finishersArr = currRoom.finishers;

          roomsController.finishGame(roomId);
          const roomsArray = roomsController.getRoomsArray();

          socketHelper.finishGame(roomId, currRoom,
            roomsArray, finishersArr);
        }
      }

      observer.invoke('LEAVE_ROOM', { roomId, username });
    }

    usersArr.splice(usersArr.indexOf(username), 1);

    const roomsArray = roomsController.getRoomsArray();
    socket.broadcast.emit('UPDATE_ROOMS', roomsArray);
  });
};
