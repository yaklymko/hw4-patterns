import { roomsMap, usersArr } from './db';
import roomHandler from './rooms';
import gameHandler from './game';
import RoomsFacade from './helpers/roomsDbFacade';
import EmitFacade from './helpers/socketEmitHelper';
import Observer from './helpers/gameObserver';
import SubscriptionFactory from './helpers/subscribersFactory';
import { CommentBotPhrases } from '../data';
import { COMMENT_GAME_UPDATE_RATE } from './config';

function onConnect(socket) {
  const { username } = socket.handshake.query;

  if (usersArr.includes(username)) {
    socket.emit('auth_error', 'user already exists');
  } else {
    usersArr.push(username);
  }

  const CommentBotObserver = new Observer(COMMENT_GAME_UPDATE_RATE);

  for (const key of CommentBotPhrases.keys()) {
    const dapendentTextFormaters = CommentBotPhrases.get(key);

    dapendentTextFormaters.forEach((func) => {
      const subscriberFunc = SubscriptionFactory.createSubscriberPartialApply(socket, func, 'COMMENT_BOT');
      CommentBotObserver.addSubscriber(subscriberFunc, key);
    });
  }

  const roomsController = new RoomsFacade(roomsMap);
  const socketHelper = new EmitFacade(socket);

  gameHandler(socket, username, usersArr, roomsController, socketHelper, CommentBotObserver);
  roomHandler(socket, username, usersArr, roomsController, socketHelper, CommentBotObserver);

  socket.emit('UPDATE_ROOMS', roomsController.getRoomsArray());
}

export default (io) => {
  io.on('connection', onConnect);
};
