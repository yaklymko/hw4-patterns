import { SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME } from './config';
import { texts } from '../data';

export default (socket, username, usersArr, roomsController, socketHelper, observer) => {
  socket.on('PLAYER_SET_READY', (isReady) => {
    const currRoomId = roomsController.getCurrentRoomId(socket);
    const currRoom = roomsController.getRoomById(currRoomId);
    if (currRoom.status !== 'lobby') {
      return;
    }
    roomsController.setPlayerReady(username, isReady, currRoomId);

    if (roomsController.arePlayersReady(currRoomId)) {
      const randomId = Math.floor(Math.random() * texts.length);

      roomsController.setStatePreTimer(currRoomId, texts[randomId].length);

      let roomArray = roomsController.getRoomsArray();

      observer.invoke('START_TIMER', { delay: SECONDS_TIMER_BEFORE_START_GAME * 1000 - 5000 });

      socketHelper.startPreGameTimer(SECONDS_TIMER_BEFORE_START_GAME, randomId,
        currRoomId, roomArray, observer,
        () => {
          roomsController.startGame(currRoomId);
          socketHelper.emitToRoom('START_GAME', { name: currRoomId, ...currRoom }, currRoomId);

          socketHelper.startGameTimer(SECONDS_FOR_GAME, currRoomId, currRoom, observer, () => {
            roomsController.setFinishersArrByProgress(currRoomId);

            const finishersArr = currRoom.finishers;
            const finishersArrayCopy = [...finishersArr];

            roomArray = roomsController.getRoomsArray();

            roomsController.finishGame(currRoomId);

            roomArray = roomsController.getRoomsArray();
            socketHelper.finishGame(currRoomId, currRoom, roomArray, finishersArrayCopy);
          });
        });
    }

    socketHelper.emitToRoom('ROOM_JOINED', { name: currRoomId, ...currRoom }, currRoomId);
  });

  socket.on('INCREASE_PROGRESS', () => {
    const currRoomId = roomsController.getCurrentRoomId(socket);
    const currRoom = roomsController.getRoomById(currRoomId);
    const currPlayer = roomsController.getPlayer(username, currRoomId);

    currPlayer.progress += 1;

    if (currPlayer.progress === currRoom.textLen) {
      if (!currRoom.finishers) {
        currRoom.finishers = [username];
      } else {
        currRoom.finishers.push(username);
      }
      observer.invoke('PLAYER_FINISHED', { player: currPlayer, delay: 1500 });

      socket.emit('UPDATE_CURR_ROOM', { name: currRoomId, ...currRoom });

      if (roomsController.arePlayersFinished(currRoomId)) {
        const finishersArr = currRoom.finishers;

        roomsController.finishGame(currRoomId);
        const roomsArray = roomsController.getRoomsArray();

        socketHelper.finishGame(currRoomId, currRoom,
          roomsArray, finishersArr);
      }
    } else if (currRoom.textLen - currPlayer.progress === 30) {
      observer.invoke('PLAYER_FINISHING', { player: currPlayer, delay: 2000 });
    }

    socketHelper.emitToRoom('UPDATE_CURR_ROOM', { name: currRoomId, ...currRoom }, currRoomId);
  });
};
