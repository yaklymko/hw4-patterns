import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../config';

export default class RoomsFacade {
    #rooms

    constructor(roomsMap) {
      this.#rooms = roomsMap;
    }

    getRoomById(roomName) {
      return this.#rooms.get(roomName);
    }

    deleteRoomById(roomName) {
      return this.#rooms.delete(roomName);
    }

    addRoom(roomName, roomInfo) {
      this.#rooms.set(roomName, roomInfo);
    }

    getRoomsArray() {
      return Array.from(this.#rooms).map(([key, value]) => ({ name: key, ...value }))
        .filter((roomInfo) => roomInfo.players.length < MAXIMUM_USERS_FOR_ONE_ROOM && roomInfo.status === 'lobby');
    }

    getCurrentRoomId(socket) {
      return Object.keys(socket.rooms).find((roomId) => this.#rooms.has(roomId));
    }

    getPlayer(username, roomId = null) {
      if (!roomId) {
        for (const key of this.#rooms.keys()) {
          const tmpRoom = this.#rooms.get(key);
          const playerId = tmpRoom.players.findIndex((p) => p.username === username);
          if (playerId !== -1) {
            return tmpRoom[playerId];
          }
        }
      } else {
        return this.getRoomById(roomId).players.find((p) => p.username === username);
      }
    }

    getRoomIdByPlayer(username) {
      for (const key of this.#rooms.keys()) {
        const tmpRoom = this.#rooms.get(key);
        const playerId = tmpRoom.players.findIndex((p) => p.username === username);

        if (playerId !== -1) {
          return key;
        }
      }
      return null;
    }

    // deletePlayerByUsername(username) {
    //
    // }

    deletePlayerByRoomId(username, roomId) {
      const room = this.getRoomById(roomId);
      const playersId = room.players.findIndex((p) => p.username === username);
      room.players.splice(playersId, 1);
    }

    setPlayerReady(username, isReady, roomId = null) {
      this.getPlayer(username, roomId).ready = isReady;
    }

    arePlayersReady(roomId) {
      const currRoom = this.getRoomById(roomId);
      return currRoom.players && currRoom.players.every((p) => p.ready);
    }

    arePlayersFinished(roomId) {
      const currRoom = this.getRoomById(roomId);
      return currRoom.players.every((p1) => currRoom.finishers.includes(p1.username));
    }

    setStatePreTimer(roomId, textLen) {
      const room = this.getRoomById(roomId);
      room.textLen = textLen;
      room.status = 'timer';
    }

    setFinishersArrByProgress(roomId) {
      const currRoom = this.getRoomById(roomId);
      currRoom.finishers = currRoom.players
        .sort((a, b) => b.progress - a.progress)
        .map((p) => p.username);
    }

    removeFromFinishers(username, gameId) {
      const currRoom = this.getRoomById(gameId);
      const index = currRoom.finishers.indexOf((usName) => usName === username);
      if (index !== -1) {
        currRoom.finishers.splice(index, 1);
      }
    }

    startGame(roomId) {
      const currRoom = this.getRoomById(roomId);
      currRoom.status = 'game';

      // eslint-disable-next-line no-param-reassign
      currRoom.players.forEach((p) => p.progress = 0);
    }

    finishGame(roomId) {
      const currRoom = this.getRoomById(roomId);

      currRoom.status = 'lobby';
      currRoom.finishers = [];

      // eslint-disable-next-line no-param-reassign
      currRoom.players.forEach((p) => {
        p.progress = 0;
        p.ready = false;
      });
    }
}
