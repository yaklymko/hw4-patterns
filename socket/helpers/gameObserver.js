export default class Observer {
    #eventMap = new Map();

    constructor(gameUpdateRate = 10) {
      this.gameUpdateRate = gameUpdateRate;
    }

    addSubscriber(subscriber, eventType = 'all') {
      const eventSubscribers = this.getEventSubscribers(eventType);

      if (eventSubscribers) {
        eventSubscribers.push(subscriber);
      } else {
        this.#eventMap.set(eventType, [subscriber]);
      }
      return subscriber;
    }

    invoke(eventType = 'all', data) {
      const eventSubscribers = this.getEventSubscribers(eventType);

      if (eventSubscribers) {
        eventSubscribers.forEach((subscriber) => {
          try {
            subscriber(data);
          } catch (err) {
            console.error(err);
          }
        });
      }
    }

    getEventSubscribers(eventType) {
      return this.#eventMap.get(eventType);
    }
}
