import { SECONDS_FOR_GAME } from '../config';

export default class EmitFacade {
    #socket

    constructor(socket) {
      this.#socket = socket;
    }

    emitToRoom(eventName, data, roomId) {
      this.#socket.to(roomId).emit(eventName, data);
      this.#socket.emit(eventName, data);
    }

    finishGame(currRoomId, currRoom, roomArray, finishersArr) {
      this.#socket.emit('FINISH_GAME', finishersArr);
      this.#socket.to(currRoomId).emit('FINISH_GAME', finishersArr);

      this.#socket.emit('UPDATE_CURR_ROOM', currRoom);
      this.#socket.to(currRoomId).emit('UPDATE_CURR_ROOM', currRoom);

      this.#socket.broadcast.emit('UPDATE_ROOMS', roomArray);
    }

    onPlayerJoin(roomId, roomInfo, roomsArray) {
      this.#socket.emit('ROOM_JOINED', { name: roomId, ...roomInfo });

      this.#socket.to(roomId).emit('UPDATE_CURR_ROOM', { name: roomId, ...roomInfo });
      this.#socket.broadcast.emit('UPDATE_ROOMS', roomsArray);
    }

    onPlayerLeave(roomsArray) {
      this.#socket.emit('ROOM_LEFT');
      this.#socket.emit('UPDATE_ROOMS', roomsArray);
      this.#socket.broadcast.emit('UPDATE_ROOMS', roomsArray);
    }

    startPreGameTimer(secondsBeforeStart, randomTextId, currRoomId, roomsArray, observer, startGameCallback) {
      let timer = secondsBeforeStart * 1000;

      this.emitToRoom('START_TIMER', randomTextId, currRoomId);
      this.emitToRoom('SECONDS_BEFORE_START', timer / 1000, currRoomId);

      this.#socket.broadcast.emit('UPDATE_ROOMS', roomsArray);

      const preGameTimerId = setInterval(() => {
        timer -= 1000;

        if (timer / 1000 <= 3) {
          observer.invoke('PRE_GAME_TIMER', { delay: 900, timer: timer / 1000 });
        }
        this.emitToRoom('SECONDS_BEFORE_START', timer / 1000, currRoomId);
      }, 1000);

      setTimeout(() => {
        clearInterval(preGameTimerId);
        startGameCallback();
      }, secondsBeforeStart * 1000);
    }

    startGameTimer(secForGame, currRoomId, currRoom, observer, finishGameCallback) {
      let timer = secForGame * 1000;

      const timerId = setInterval(() => {
        if ((timer / 1000) % observer.gameUpdateRate === 0) {
          observer.invoke('UPDATE_GAME', { users: currRoom.players, delay: 5000 });
        }

        if (currRoom.status === 'game') {
          timer -= 1000;
          this.emitToRoom('GAME_SECONDS_LEFT', timer / 1000, currRoomId);
        } else {
          clearInterval(timerId);
          currRoom.clearGameTimer();
        }
      }, 1000);

      const gameTimerId = setTimeout(() => {
        if (currRoom.status === 'game') {
          finishGameCallback();
        }
        clearInterval(timerId);
      }, SECONDS_FOR_GAME * 1000);

      currRoom.clearGameTimer = () => clearInterval(gameTimerId);
    }
}
