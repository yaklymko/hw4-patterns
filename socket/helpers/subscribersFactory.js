import curry from 'lodash.curry';

function createSocketEmit(socket, emitEventName, formatPhraseFunction, data = {}) {
  socket.emit(emitEventName, { text: formatPhraseFunction(data), delay: data.delay });
  socket.to(data.roomId).emit(emitEventName, { text: formatPhraseFunction(data), delay: data.delay });
}

export default class PhrasesFactory {
  // Use these method as class factory, but could also convert to functional

  // Two ways of doing the same
  // I prefer partial apply

  static createSubscriberPartialApply(socket, formatPhraseFunction, emitEventName) {
    return (data = {}) => {
      createSocketEmit(socket, emitEventName, formatPhraseFunction, data);
    };
  }

  // won't use it
  static createSubscriberCurry(socket, formatPhraseFunction, emitEventName) {
    const curried = curry(createSocketEmit);
    return curried(socket)(emitEventName)(formatPhraseFunction);
  }
}
